// SPDX-License-Identifier:	GPL-2.0
/* Copyright 2018 Panfrost Team */

#ifndef __PANFROST_DRM_H__
#define __PANFROST_DRM_H__

#include "drm.h"

#if defined(__cplusplus)
extern "C" {
#endif

/* timeouts are specified in clock-monotonic absolute times (to simplify
 * restarting interrupted ioctls).  The following struct is logically the
 * same as 'struct timespec' but 32/64b ABI safe.
 */
struct drm_panfrost_timespec {
	__s64 tv_sec;          /* seconds */
	__s64 tv_nsec;         /* nanoseconds */
};

#define PANFROST_INFO_GPU_MALI_T760 0x00
#define PANFROST_INFO_GPU_MALI_T860 0x01

struct drm_panfrost_info {
	__u32 gpu_id;	/* out */
};

struct drm_panfrost_gem_new {
	__u64 size;           /* in */
	__u32 flags;          /* in, mask of ETNA_BO_x */
	__u32 handle;         /* out */
	/**
	 * Returned offset for the BO in the GPU address space.  This offset
	 * is private to the DRM fd and is valid for the lifetime of the GEM
	 * handle.
	 *
	 * This offset value will always be nonzero, since various HW
	 * units treat 0 specially.
	 */
	__u64 offset;
};
struct drm_panfrost_gem_info {
	__u32 handle;         /* in */
	__u32 pad;
	__u64 offset;         /* out, offset to pass to mmap() */
};

/**
 * Returns the offset for the BO in the GPU address space for this DRM fd.
 * This is the same value returned by drm_panfrost_gem_new, if that was called
 * from this DRM fd.
 */
struct drm_panfrost_get_bo_offset {
	__u32 handle;
	__u32 pad;
	__u64 offset;
};


#define ETNA_PREP_READ        0x01
#define ETNA_PREP_WRITE       0x02
#define ETNA_PREP_NOSYNC      0x04

struct drm_panfrost_gem_cpu_prep {
	__u32 handle;         /* in */
	__u32 op;             /* in, mask of ETNA_PREP_x */
	struct drm_panfrost_timespec timeout;   /* in */
};

struct drm_panfrost_gem_cpu_fini {
	__u32 handle;         /* in */
	__u32 flags;          /* in, placeholder for now, no defined values */
};

/*
 * Cmdstream Submission:
 */

/* The value written into the cmdstream is logically:
 * relocbuf->gpuaddr + reloc_offset
 *
 * NOTE that reloc's must be sorted by order of increasing submit_offset,
 * otherwise EINVAL.
 */
struct drm_panfrost_gem_submit_reloc {
	__u32 submit_offset;  /* in, offset from submit_bo */
	__u32 reloc_idx;      /* in, index of reloc_bo buffer */
	__u64 reloc_offset;   /* in, offset from start of reloc_bo */
	__u32 flags;          /* in, placeholder for now, no defined values */
};

/* Each buffer referenced elsewhere in the cmdstream submit (ie. the
 * cmdstream buffer(s) themselves or reloc entries) has one (and only
 * one) entry in the submit->bos[] table.
 *
 * As a optimization, the current buffer (gpu virtual address) can be
 * passed back through the 'presumed' field.  If on a subsequent reloc,
 * userspace passes back a 'presumed' address that is still valid,
 * then patching the cmdstream for this entry is skipped.  This can
 * avoid kernel needing to map/access the cmdstream bo in the common
 * case.
 */
#define ETNA_SUBMIT_BO_READ             0x0001
#define ETNA_SUBMIT_BO_WRITE            0x0002
struct drm_panfrost_gem_submit_bo {
	__u32 flags;          /* in, mask of ETNA_SUBMIT_BO_x */
	__u32 handle;         /* in, GEM handle */
	__u64 presumed;       /* in/out, presumed buffer address */
};

/* Each cmdstream submit consists of a table of buffers involved, and
 * one or more cmdstream buffers.  This allows for conditional execution
 * (context-restore), and IB buffers needed for per tile/bin draw cmds.
 */
#define ETNA_SUBMIT_NO_IMPLICIT         0x0001
#define ETNA_SUBMIT_FENCE_FD_IN         0x0002
#define ETNA_SUBMIT_FENCE_FD_OUT        0x0004
#define ETNA_SUBMIT_FLAGS		(ETNA_SUBMIT_NO_IMPLICIT | \
					 ETNA_SUBMIT_FENCE_FD_IN | \
					 ETNA_SUBMIT_FENCE_FD_OUT)
struct drm_panfrost_gem_submit {
	__u32 fence;          /* out */
	__u32 exec_state;     /* in, initial execution state (ETNA_PIPE_x) */
	__u32 nr_bos;         /* in, number of submit_bo's */
	__u32 nr_relocs;      /* in, number of submit_reloc's */
	__u32 stream_size;    /* in, cmdstream size */
	__u64 bos;            /* in, ptr to array of submit_bo's */
	__u64 relocs;         /* in, ptr to array of submit_reloc's */
	__u64 stream;         /* in, ptr to cmdstream */
	__u32 flags;          /* in, mask of ETNA_SUBMIT_x */
	__s32 fence_fd;       /* in/out, fence fd (see ETNA_SUBMIT_FENCE_FD_x) */
	__u64 pmrs;           /* in, ptr to array of submit_pmr's */
	__u32 nr_pmrs;        /* in, number of submit_pmr's */
	__u32 pad;
};



#define DRM_PANFROST_INFO		0x00
#define DRM_PANFROST_GEM_NEW		0x01
#define DRM_PANFROST_GEM_INFO		0x02
#define DRM_PANFROST_GEM_CPU_PREP	0x03
#define DRM_PANFROST_GEM_CPU_FINI	0x04
#define DRM_PANFROST_GET_BO_OFFSET	0x05

#define DRM_IOCTL_PANFROST_INFO		DRM_IOR(DRM_COMMAND_BASE + DRM_PANFROST_INFO, struct drm_panfrost_info)
#define DRM_IOCTL_PANFROST_GEM_NEW	DRM_IOWR(DRM_COMMAND_BASE + DRM_PANFROST_GEM_NEW, struct drm_panfrost_gem_new)
#define DRM_IOCTL_PANFROST_GEM_INFO	DRM_IOWR(DRM_COMMAND_BASE + DRM_PANFROST_GEM_INFO, struct drm_panfrost_gem_info)
#define DRM_IOCTL_PANFROST_GEM_CPU_PREP	DRM_IOW(DRM_COMMAND_BASE + DRM_PANFROST_GEM_CPU_PREP, struct drm_panfrost_gem_cpu_prep)
#define DRM_IOCTL_PANFROST_GEM_CPU_FINI	DRM_IOW(DRM_COMMAND_BASE + DRM_PANFROST_GEM_CPU_FINI, struct drm_panfrost_gem_cpu_fini)
#define DRM_IOCTL_PANFROST_GET_BO_OFFSET DRM_IOW(DRM_COMMAND_BASE + DRM_PANFROST_GET_BO_OFFSET, struct drm_panfrost_get_bo_offset)

#if defined(__cplusplus)
}
#endif

#endif /* __PANFROST_DRM_H__ */
