#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <xf86drm.h>

#include "panfrost_drm.h"

void pan_get_info(int fd)
{
	struct drm_panfrost_info info = {};
	int ret;


	ret = drmIoctl(fd, DRM_IOCTL_PANFROST_INFO, &info);
	if (ret) {
                fprintf(stderr, "DRM_IOCTL_PANFROST_INFO failed: %d\n", errno);
		return;
	}

        printf("DRM_IOCTL_PANFROST_INFO: gpu_id=%u\n", info.gpu_id);
}

void pan_bo_create(int fd, size_t size)
{
	struct drm_panfrost_gem_new gem_new = {
			.size = size,
			.flags = 0,  // TODO figure out proper flags..
	};
	struct drm_panfrost_gem_info gem_info = {};
	int ret;
	void *buf;


	ret = drmIoctl(fd, DRM_IOCTL_PANFROST_GEM_NEW, &gem_new);
	if (ret) {
                fprintf(stderr, "DRM_IOCTL_PANFROST_GEM_NEW failed: %d\n", errno);
		return;
	}

        printf("DRM_IOCTL_PANFROST_GEM_NEW: offset=%llu, handle=%u\n", gem_new.offset, gem_new.handle);

	// TODO map and unmap on demand?
	gem_info.handle = gem_new.handle;
	ret = ioctl(fd, DRM_IOCTL_PANFROST_GEM_INFO, &gem_info, sizeof(gem_info));
	if (ret) {
                fprintf(stderr, "DRM_IOCTL_PANFROST_GEM_INFO failed: %d\n", errno);
		return;
	}

        buf = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED,
                       fd, gem_info.offset);
        if (buf == MAP_FAILED) {
                fprintf(stderr, "mmap failed\n");
		return;
	}
	((int *)buf)[0] = 0x12345678;
}

int main(void)
{
	int fd = drmOpenWithType("panfrost", NULL, DRM_NODE_PRIMARY);
	if (fd < 0)
		return -1;

	pan_get_info(fd);
	pan_bo_create(fd, 0x10000);
	pan_bo_create(fd, 0x10000);
	pan_bo_create(fd, 0x10000);

}
